
# Setup

In mongo container create indexes:

    mongo scitizen
    // For mongo 2.2
    >db.images.ensureIndex( { "fields.location" : "2d" })
    // GeoJSON in loc, for mongo 2.4
    >db.images.ensureIndex({ "fields.loc" : "2dsphere" })

In Influxdb container, create the *scitizen* database

# Running

Use docker-compose provided in git repository https://bitbucket.org/osallou/scitizen/src
Create a .env file next to docker-compose.yml file with env variables:


    SCITIZEN_DIR=path_to_persistent_data/scitizen-data
    SCITIZEN_GOOGLE_APIKEY=api_key_for_google_maps
    COMPOSE_PROJECT_NAME=scitizen
    SCITIZEN_ADMIN_PASSWORD=password_of_admin

You can override default public URL to reach web site with env variable SCITIZEN_PUBLIC_URL (http://myapp)
